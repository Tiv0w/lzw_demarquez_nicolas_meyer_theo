#include "tools.h"
#include "global.h"
#include "codec_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


/**
 * Procédure de transformation d'un couple d'entiers en un triplet d'octets.
 */
void coder_couple(int* couple, t_octet* triplet) {
    t_octet first, second, third;

    third = couple[1] & 0xff;

    second = ecrire_quartetInf(0, (t_octet)(couple[1] >> 8));
    second = ecrire_quartetSup(second, lire_quartetInf(couple[0] & 0xff) << 4);

    first = couple[0] >> 4;

    triplet[0] = first;
    triplet[1] = second;
    triplet[2] = third;
}


/**
 * Procédure de transformation d'un triplet d'octet en un couple d'entiers.
 */
void decoder_triplet(t_octet* triplet, int* couple) {
    int first, second;

    second = (int)ecrire_quartetInf(0, lire_quartetInf(triplet[1]));
    second = second << 8;
    second = second | triplet[2];

    first = (int)triplet[0] << 4;
    first = first | (lire_quartetSup(triplet[1]) >> 4);

    couple[0] = first;
    couple[1] = second;
}


/**
 * Procédure d'émission d'un code du codeur vers le décodeur.
 */
void emit_code(const int code, int* communication_pipe) {
    static int counter = 0;
    static int couple[2];
    counter++;

    if (counter == 2) {
        counter = 0;
        couple[1] = code;

        t_octet triplet[3];
        coder_couple(couple, triplet);
        /* printf("ENVOI: %d %d\n", couple[0], couple[1]); */
        if (write(communication_pipe[1], triplet, sizeof(triplet)) == -1) error("Cannot write to pipe");
    } else {
        couple[0] = code;
    }
}


/**
 * Procédure de réception d'un code provenant du codeur vers le décodeur.
 */
void receive_couple(int* couple, int* communication_pipe) {
    t_octet triplet[3];

    if (read(communication_pipe[0], triplet, sizeof(triplet)) == -1) error("Cannot write to pipe");

    decoder_triplet(triplet, couple);
    /* printf("RECU: %d %d\n", couple[0], couple[1]); */
}


/**
 * Fonction qui retourne le code correspondant à la chaîne passée en paramètre dans le dictionnaire.
 * Renvoie le code, ou -1 si la chaîne n'est pas dans le dictionnaire.
 */
int get_code_of_str_from_dictionary(char* str, t_dictionary dictionnary) {
    for (int i = 0; i <= dictionnary.max_index; ++i) {
        /* if (i > 255) printf("%s : ", dictionnary.values[i]); */
        if (strcmp(str, dictionnary.values[i]) == 0)
            return i;
    }
    return -1;
}


/**
 * Fonction qui ajoute une nouvelle chaîne dans le dictionnaire.
 * Renvoie le dictionnaire modifié.
 */
t_dictionary add_str_to_dictionary(char* str, t_dictionary dictionary) {
    dictionary.max_index++;
    dictionary.values[dictionary.max_index] = str;
    return dictionary;
}


/**
 * Fonction qui renvoie la taille d'un fichier.
 */
long get_size_from_file_stream(FILE* fp) {
    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return file_size;
}


/**
 * Procédure qui charge un fichier dans une chaîne.
 */
void load_file_characters(FILE* fp, char* input) {
    int i;
    for (i = 0; !feof(fp); i++) {
        input[i] = fgetc(fp);
    }
    input[i - 1] = '\0';
}


/**
 * Procédure d'affichage d'erreur.
 * Arrête immédiatement le programme.
 */
void error(char* message) {
    printf("Error: %s.\n", message);
    exit(1);
}
