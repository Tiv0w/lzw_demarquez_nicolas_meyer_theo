#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "global.h"
#include "tools.h"


void afficher_octet(
    const t_octet Oc ,
    const char * ident ,
    const int mode ) {
    static char buffer[8];
    int n;

    printf( "%s = 0b" , ident );
    for( n = 0 ; n < 8 ; n++ ) {
        t_octet bit = (1 << n);
        buffer[7-n] = ((Oc & bit) ? '1' : '0');
    }
    printf( "%s" , buffer );
    if( mode == 1 )
        printf( " = %u" , Oc );
    printf( "\n" );
}


t_octet lire_octet() {
    int c;

    printf("Entrez un octet : ");
    scanf(" %d" , &c);

    // les seules valeurs possibles pour un octet
    assert( -1 < c && c < 256 );

    return (t_octet) c;
}


int extraire_bit(const t_octet Oc, const int n) {
    assert( -1 < n && n < 8 );

    t_octet Dn = 1 << n;

    return ((Oc & Dn) ? 1 : 0);
}


t_octet modifier_bit(const t_octet Oc, const int n) {
    assert( -1 < n && n < 8 );

    t_octet Dn = extraire_bit(Oc, n) << n;

    return (Oc ^ Dn);
}


t_octet lire_quartetInf(t_octet O) {
    static t_octet masque = 0x0f;

    return (t_octet) (O & masque);
}


t_octet lire_quartetSup(t_octet O) {
    static t_octet masque = 0xf0;

    return (t_octet) (O & masque);
}


t_octet ecrire_quartetInf(t_octet O, t_octet qi) {
    static t_octet masque = 0xf0;

    return (t_octet) ((O & masque) | qi);
}


t_octet ecrire_quartetSup( t_octet O, t_octet qs ) {
    static t_octet masque = 0x0f;

    return (t_octet) ((O & masque) | qs);
}
