#ifndef _GLOBAL_
#define _GLOBAL_

// byte type definition
typedef unsigned char t_octet;

// byte constants
extern const t_octet Z;
extern const t_octet U;

#endif
