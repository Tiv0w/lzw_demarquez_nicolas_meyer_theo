BINARY = codec_lzw
CC = gcc
CFLAGS = -Wall -Wextra -g

OBJS = codec_utils.o tools.o
DEPS = global.h


all: $(BINARY)

$(BINARY): $(BINARY).c $(OBJS)
	$(CC) $(CFLAGS) -o $(BINARY) $(BINARY).c $(OBJS)

%.o: %.c %.h $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f *.o $(BINARY)

run:
	./$(BINARY)
