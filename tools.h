#ifndef _TOOLS_
#define _TOOLS_

#include "global.h"

/**
 * signature des fonctions bit à bit
 */

/**
 * @brief afficher un octet au format binaire
 * Entrées :
 * 	+ Oc, l'octet à afficher
 * 	+ ident, son identifiant
 * 	+	mode == 1, affiche également la valeur en décimal
 */
void afficher_octet(
    const t_octet Oc,
    const char *ident,
    const int mode);

/**
 * @brief lire un octet en s'assurant que sa valeur soit bien comprise
 * entre les valeurs 0 et 255 incluses
 */
t_octet lire_octet();


/**
 * @brief extraire la valeur du n^{ième} bit de l'octet Oc
 * avec n comprise entre 0 et 7
 */
int extraire_bit (const t_octet Oc, const int n);


/**
 * @brief modifier la valeur du n^{ième} bit de l'octet OC
 * avec n compris entre 0 et 7
 */
t_octet modifier_bit(const t_octet Oc, const int n);

/**
 *
 * @brief extraire les valeurs des 4 bits de poids faible de l'octet Oc
 */
t_octet lire_quartetInf(t_octet O);

/**
 *
 * @brief extraire les valeurs des 4 bits de poids fort de l'octet Oc
 */
t_octet lire_quartetSup(t_octet O);

/**
 *
 * @brief modifier les valeurs des 4 bits de poids faible de l'octet Oc
 */
t_octet ecrire_quartetInf(t_octet O, t_octet qi);

/**
 *
 * @brief modifier les valeurs des 4 bits de poids fort de l'octet Oc
 */
t_octet ecrire_quartetSup(t_octet O, t_octet qs);

#endif
