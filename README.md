# LZW_DEMARQUEZ_NICOLAS_MEYER_THEO

Un sacré projet, avec plein d'explosions.

Projet de compression de données de DEMARQUEZ Nicolas et MEYER Théo.


## Introduction rapide

Ceci est notre projet de l'UE Compression de Données de L3.

L'objectif est d'implémenter l'algorithme de compression de données LZW. Il faut écrire le codeur et le décodeur, le tout en langage C.

Les consignes étaient les suivantes :
- langage C ;
- communication via pipe ou socket entre le codeur et le décodeur ;
- codes générés codés sur 12 bits ;
- du code lisible et documenté.


### Difficultés rencontrées

Les difficultés que nous avons rencontrées sont les suivantes :
- Plusieurs fois des erreurs de notre part dans l'implémentation de l'algorithme ;
- Des problèmes de gestion de la mémoire ;
- Les caractères UTF-8 différents de ceux de la table ASCII sont codés sur plusieurs octets, cela a posé quelques soucis ;
- La mise en place du pipe de communication entre le codeur et le décodeur a mis un peu de temps.

Fort heureusement, nous sommes venus à bout de tous ces soucis !


### Les algorithmes utilisés

Voici les pseudo-codes des algorithmes que nous avons utilisés. Ils sont un petit peu différents de ceux qui ont été fournis, mais le principe est exactement le même, et nous avons pu plus simplement les implémenter.

##### Le codeur

*(Les lettres majuscules représentent des chaînes, celles minuscules un seul caractère. L'opérateur `+` entre une chaîne et un caractère matérialise la concaténation.)*

```
Initialiser le dictionnaire initial avec les 256 premières valeurs.
S <- [vide]

Tant que l'entrée n'est pas vide :
    m <- prochain caractère de l'entrée

    Si (S + m) est dans le dictionnaire :
        S <- S + m
	Sinon
		Ajouter (S + m) dans le dictionnaire.
		Rajouter le code de S à la sortie.
		S <- [m]
	Fin Si
Fin Tant que

Rajouter le code de S à la sortie.
```

##### Le décodeur

```
Initialiser le dictionnaire initial avec les 256 premières valeurs.
A <- premier code dans l'entrée
Rajouter la signification de A à la sortie.

Tant que l'entrée n'est pas vide :
    CODE <- prochain code dans l'entrée

    Si CODE est dans le dictionnaire :
		m <- premier caractère de la signification de CODE
		Rajouter la signification de CODE à la sortie.
        Ajouter ((signification de A) + m) au dictionnaire.
	Sinon
	 	m <- premier caractère de la signification de A
		Rajouter ((la signification de A) + m) à la sortie.
		Ajouter ((la signification de A) + m) au dictionnaire.
	Fin Si

	A <- CODE
Fin Tant que
```


Ce sont ici les grandes lignes des algorithmes que nous avons utilisés. Les détails d'implémentation ne sont pas pris en compte dans ces pseudo-codes (comme par exemple la remise en l'état initial du dictionnaire quand il est plein,...).


## Compilation

Le code source est fourni avec un Makefile au format GNU, ce dernier peut donc être utilisé avec l'utilitaire GNU `make`.

Pour compiler les sources :
```sh
$ make
```

Au besoin :
```sh
$ make clean && make
```


## Utilisation du programme

Le programme s'utilise de la manière suivante :
```sh
$ ./codec_lzw INPUT OUTPUT
```
où INPUT est le fichier d'entrée et OUTPUT le fichier de sortie.


Pour tester le fonctionnement correct du programme :
```sh
$ diff INPUT OUTPUT -s
```
Si tout a correctement fonctionné, la dernière commande devrait dire que les deux fichiers sont identiques.


#### Voilà, c'était sympa non ?

DEMARQUEZ Nicolas et MEYER Théo
