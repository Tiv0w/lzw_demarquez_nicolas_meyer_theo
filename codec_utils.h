#pragma once
#include "global.h"
#include <stdio.h>


typedef struct {
    char** values;
    int max_index;
} t_dictionary;


void coder_couple(int* couple, t_octet* triplet);
void decoder_triplet(t_octet* triplet, int* couple);

void emit_code(const int code, int* communication_pipe);
void receive_couple(int* couple, int* communication_pipe);
int get_code_of_str_from_dictionary(char* str, t_dictionary dictionnary);
t_dictionary add_str_to_dictionary(char* str, t_dictionary dictionary);

long get_size_from_file_stream(FILE* fp);
void load_file_characters(FILE* fp, char* input);

void error(char* message);
