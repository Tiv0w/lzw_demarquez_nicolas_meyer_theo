#include "codec_lzw.h"
#include "codec_utils.h"
#include "global.h"
#include "tools.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>



int main(int argc, char* argv[]) {
    // Setup for use in command-line
    if (argc < 3) {
        printf("Usage: <program> INPUT OUTPUT\nWhere INPUT is the file to encode, and OUTPUT the file to write the decoded data to.\n");
        exit(1);
    }
    char* input_filename = argv[1];
    char* output_filename = argv[2];


    char* input;
    char* output;

    // Ouvre le fichier et charge son contenu dans la variable `input`
    FILE* input_file_stream = fopen(input_filename, "r");
    long file_size = get_size_from_file_stream(input_file_stream);
    input = (char*)malloc(file_size * sizeof(char));
    load_file_characters(input_file_stream, input);
    fclose(input_file_stream);

    // On initialise la sortie à la même taille que l'entrée
    output = (char*)malloc(strlen(input) * sizeof(char));


    // Mets en place le pipe de communication entre le codeur et le décodeur
    int communication_pipe[2];
    if (pipe(communication_pipe) == -1) error("Cannot init pipe");


    // On fork le programme, le fork sera le décodeur, tandis que le programme principal sera le codeur.
    switch (fork()) {
    case -1:
        error("Cannot fork");
        break;
    case 0:
        // On décode en direct le code, et on le place dans la chaîne `output`.
        decodeur_lzw(output, communication_pipe);
        // On écrit la chaîne de résultat dans le fichier de sortie.
        FILE* output_file_stream = fopen(output_filename, "w");
        fprintf(output_file_stream, "%s", output);
        fclose(output_file_stream);
        printf("Décodage réussi.\n");
        exit(0);
        break;
    }

    // On lance le codeur, qui va envoyer en direct les codes générés au décodeur.
    codeur_lzw(input, communication_pipe);

    printf("Codage réussi.\n");
    return 0;
}


void codeur_lzw(char* input, int* communication_pipe) {
    t_dictionary dictionary;
    dictionary.values = (char**)malloc(0x1000 * sizeof(char*));
    for (int i = 0; i < 0x1000; ++i)
        dictionary.values[i] = (char*)malloc(0x1000 * sizeof(char));

    // On remplit le dictionnaire de toutes les "chaines" de 1 caractère de la table ASCII
    for (int i = 0; i < 256; ++i)
        dictionary.values[i][0] = (char)i;
    dictionary.max_index = 255;

    // On ferme la sortie du pipe (inutile pour le codeur).
    if (close(communication_pipe[0]) == -1) error("Cannot close pipe output");

    // On initialise la chaine S
    char* current_string = (char*)malloc(strlen(input) * sizeof(char));
    current_string[0] = input[0];
    current_string[1] = '\0';


    for (int input_index = 1; input[input_index] != '\0'; input_index++) {
        // Si le dictionnaire a atteint le nombre maximal de mots qu'il peut contenir,
        // on le remet à l'état initial (uniquement avec les 256 premiers codes).
        if (dictionary.max_index == 0xfff) {
            for (int i = 256; i < 0x1000; ++i)
                dictionary.values[i][0] = '\0';
            dictionary.max_index = 255;
        }

        // On stocke le prochain caractère sous forme de string contenant uniquement le caractère et un \0
        char* next_char = (char*)malloc(2 * sizeof(char));
        next_char[0] = input[input_index];
        next_char[1] = '\0';

        // La chaîne courante à laquelle on a concaténé le prochain caractère
        char* current_string_and_next_char = (char*)malloc(strlen(current_string) * sizeof(char));
        strcpy(current_string_and_next_char, current_string);
        strcat(current_string_and_next_char, next_char);

        const int code = get_code_of_str_from_dictionary(current_string_and_next_char, dictionary);

        if (code != -1) {
            current_string = current_string_and_next_char;
        } else {
            dictionary = add_str_to_dictionary(current_string_and_next_char, dictionary);
            const int code_of_current_string = get_code_of_str_from_dictionary(current_string, dictionary);
            emit_code(code_of_current_string, communication_pipe);
            current_string = next_char;
        }
    }

    const int code_of_current_string = get_code_of_str_from_dictionary(current_string, dictionary);
    emit_code(code_of_current_string, communication_pipe);

    // Permet de s'assurer que le décodeur a bien reçu un code 0 pour terminer le décodage
    emit_code(0, communication_pipe); emit_code(0, communication_pipe);
    //

    if (close(communication_pipe[1]) == -1) error("Cannot close pipe input");
}


void decodeur_lzw(char* output, int* communication_pipe) {
    t_dictionary dictionary;
    dictionary.values = (char**)malloc(0x1000 * sizeof(char*));
    for (int i = 0; i < 0x1000; ++i)
        dictionary.values[i] = (char*)malloc(0x1000 * sizeof(char));

    // On remplit le dictionnaire de toutes les "chaines" de 1 caractère de la table ASCII
    for (int i = 0; i < 256; ++i)
        dictionary.values[i][0] = (char)i;
    dictionary.max_index = 255;

    // On prépare tout ce qui est réception
    if (close(communication_pipe[1]) == -1) error("Cannot close pipe input");
    int input_couple[2];
    receive_couple(input_couple, communication_pipe);

    // On prend le tout premier code de l'entrée
    int current_code = input_couple[0];
    // et on ajoute son caractère correspondant en sortie
    output[0] = (char)current_code;
    output[1] = '\0';


    int need_to_receive_input = 1;
    while (current_code != 0) {
        // Si le dictionnaire a atteint le nombre maximal de mots qu'il peut contenir,
        // on le remet à l'état initial (uniquement avec les 256 premiers codes).
        if (dictionary.max_index == 0xfff) {
            for (int i = 256; i < 0x1000; ++i)
                dictionary.values[i][0] = '\0';
            dictionary.max_index = 255;
        }

        int next_code = input_couple[need_to_receive_input];

        // On récupère la signification de current_code
        char* current_code_meaning = (char*)malloc(sizeof(dictionary.values[current_code]));
        strcpy(current_code_meaning, dictionary.values[current_code]);

        // Si le next_code est dans le dictionnaire
        if (next_code <= dictionary.max_index) {
            // On récupère la signification de next_code
            char* next_code_meaning = (char*)malloc(sizeof(dictionary.values[next_code]));
            strcpy(next_code_meaning, dictionary.values[next_code]);

            // On ajoute la signification de next_code à la sortie
            strcat(output, next_code_meaning);

            // On récupère le premier caractère de la signification de next_code
            // (sous forme de string pour se simplifier certaines manipulations)
            char first_char_of_next_code_meaning[2] = "\0\0";
            first_char_of_next_code_meaning[0] = next_code_meaning[0];

            // On ajoute le 1er caractère de la signification de next_code à la signification de current_code
            strcat(current_code_meaning, first_char_of_next_code_meaning);
            // Puis on ajoute cette concaténation au dictionnaire
            dictionary = add_str_to_dictionary(current_code_meaning, dictionary);
        } else {
            // On récupère le premier caractère de la signification de current_code
            // (sous forme de string pour se simplifier certaines manipulations)
            char first_char_of_current_code_meaning[2] = "\0\0";
            first_char_of_current_code_meaning[0] = current_code_meaning[0];

            // On ajoute le 1er caractère de la signification de current_code à elle même
            strcat(current_code_meaning, first_char_of_current_code_meaning);

            // Puis on ajoute cette concaténation à la sortie et au dictionnaire
            strcat(output, current_code_meaning);
            dictionary = add_str_to_dictionary(current_code_meaning, dictionary);
        }

        current_code = next_code;

        // On reçoit les deux nouvelles entrées du pipe 1 tour de boucle sur 2
        if (need_to_receive_input) receive_couple(input_couple, communication_pipe);
        need_to_receive_input = !need_to_receive_input;
    }

    if (close(communication_pipe[0]) == -1) error("Cannot close pipe output");
}
